const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const session = require('express-session');
const bodyParser = require('body-parser');
const path = require('path');
const socketIO = require('socket.io');

const errorMiddleware = require('./middleware/error');
const authMiddleware = require('./middleware/authorize');
const refMiddleware = require('./middleware/refresh');

const app = express();

app.use(session({
  secret: '142e6ecf42884f03',
  resave: false,
  saveUninitialized: true,
}));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const PORT = process.env.PORT || 8080;

app.use('/login', require('./routes/login'));
app.use('/signup', require('./routes/signup'));
app.use('/getItems', require('./routes/getItems'));
app.use('/rent', authMiddleware.authorize(), require('./routes/rent'));
app.use('/user', authMiddleware.authorize(), require('./routes/user'));
app.use('/review', require('./routes/review'));
app.use('/getadmintoken', require('./routes/adminlogin'));

app.use('/login', (req, res) => res.sendFile('/public/login.html', { root: __dirname }));
app.use('/admin', (req, res) => res.sendFile('/public/admin.html', { root: __dirname }));

// React
app.use(express.static(path.join(__dirname, 'build')));
app.get('/movieSearch', (req, res) => {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

const server = app.listen(PORT, () => {
  console.log('Server listening...');
});

const io = socketIO.listen(server);

app.use('/items',
  authMiddleware.authorize('admin'),
  (res, req, next) => refMiddleware(res, req, next, io),
  require('./routes/items')
);

app.use('/refresh', (req, res) => { 
  io.emit('refresh'); 
  return res.json('Refreshed all connected devices')
})

app.use(errorMiddleware);

io.on('connection', (socket) => {
  console.log('User connected!');
  socket.on('reserve', (id) => {
    io.emit('setUnavailable', id);
  });
  socket.on('cancel', (id) => {
    io.emit('setAvailable', id);
  });
});

module.exports.app = app;
