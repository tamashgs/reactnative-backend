const onFinished = require('on-finished')

module.exports = (req, res, next, io) => {

  if (req.method!="GET")
    onFinished(res, (err) => {
      console.log('Refreshed all connected devices')
      io.emit('refresh');
    })

  next()
};