const jwt = require('jsonwebtoken');

const jwtSecretKey = '142e6ecf42884f03';

module.exports.authorize = (role = 'user') => (req, res, next) => {
  const token = req.headers.authorization;
  jwt.verify(token, jwtSecretKey, (error, auth) => {
    if (error) {
      return res.status(403).json({ message: 'You are not logged in!' });
    }
    req.session.username = auth;
    next();
  });
};
