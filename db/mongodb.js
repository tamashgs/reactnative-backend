const { ObjectId } = require('mongodb');
const { MongoClient } = require('mongodb');

const uri = 'mongodb+srv://admin:admin@reactnative-y43uo.mongodb.net/test?retryWrites=true&w=majority';

module.exports = {
  getAll(dbName, collectionName) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db(dbName);
        const collection = db.collection(collectionName);
        collection.find().toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
  getUserByName(name) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('users');
        collection.find({ username: name }).toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
  addUser(user) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('users');

        collection.insertOne(user, (err, records) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(records.insertedId);
        });
      });
    });
  },
  updateUserRentTime(username, time) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('users');
        collection.updateOne(
          { username },
          {
            $set: {
              rentTime: time,
            },
          },
          (err, res) => {
            client.close();
            if (err) { console.log(err); reject(err); } else resolve(res);
          },
        );
      });
    });
  },
  updateUserRental(username, time, itemId, historyId) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('users');
        collection.updateOne(
          { username },
          {
            $set: {
              rentId: historyId,
              rentTime: time,
              rentItemId: itemId,
            },
          },
          (err, res) => {
            client.close();
            if (err) { console.log(err); reject(err); } else resolve(res);
          },
        );
      });
    });
  },
  getAllLocations() {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.find().toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
  addLocation(location) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.insertOne(location, (err, records) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(records.insertedId);
        });
      });
    });
  },
  updateLocationAvailability(id, available) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.updateOne(
          { _id: ObjectId(id) },
          {
            $set: {
              available,
            },
          },
          (err, res) => {
            client.close();
            if (err) { console.log(err); reject(err); } else resolve(res);
          },
        );
      });
    });
  },
  addToHistory(username, itemID, startTime, startLoc) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) {
          reject(err);
          client.close();
        } else  {
          const historyItem = {
            username,
            itemID,
            startTime,
            startLoc,
          };
          const db = client.db('RNdb');
          const collection = db.collection('history');
          collection.insertOne(historyItem, (err, records) => {
            client.close();
            if (err) reject(err);
            else resolve(records.insertedId);
          });
        }
      });
    });
  },
  updateHistory(username, rentID, itemID, endTime, endLoc, payed) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('history');
        collection.updateOne(
          { _id: rentID },
          {
            $set: {
              endTime,
              endLoc,
              payed,
            },
          },
          (err, res) => {
            client.close();
            if (err) reject(err);
            else resolve(res);
          },
        );
      });
    });
  },
  getHistoryByUsername(username) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('history');
        collection.find({ username }).toArray((err, docs) => {
          client.close();
          if (err) reject(err);
          else resolve(docs);
        });
      });
    });
  },
  updateItem(id, name, latitude, longitude, available, code, price, type) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        const item = {
          name,
          latitude,
          longitude,
          available,
          code,
          price,
          type,
        };
        // eslint-disable-next-line no-restricted-syntax
        for (const propName in item) {
          if (item[propName] === null || item[propName] === undefined) {
            delete item[propName];
          }
        }
        collection.updateOne(
          { _id: ObjectId(id) },
          {
            $set: item,
          },
          (err2, res) => {
            client.close();
            if (err2) reject(err2);
            else resolve(res);
          },
        );
      });
    });
  },
  addItem(item) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.insertOne(item, (err2, res) => {
          client.close();
          if (err2) reject(err2);
          else resolve(res);
        });
      });
    });
  },
  deleteItem(id) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.deleteOne({ _id: ObjectId(id) }, (err2, res) => {
          client.close();
          if (err2) reject(err2);
          else resolve(res);
        });
      });
    });
  },
  getItem(id) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.find({ id: id }).toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
  getAllItemsWithRate(){
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('locations');
        collection.aggregate([
          { 
            $addFields: 
              { 
                "itemId": { "$toString": "$_id" }
              }
          },
          {
            $lookup:
              {
                from: "review",
                localField: "itemId",
                foreignField: "itemId",
                as: "reviews"
              }
          },
          {
            $addFields:
              {
                rate: { $round : [ { $avg: "$reviews.rating" }, 1 ] },
              }
          },
          { 
            $unset: [ "reviews", "itemId" ]
          }
          ]
       ).toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
  addReview(review) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('review');
        collection.insertOne(review, (err2, res) => {
          client.close();
          if (err2) reject(err2);
          else resolve(res);
        });
      });
    });
  },
  getReviewsByItemId(id) {
    const client = new MongoClient(uri, { useUnifiedTopology: true }, { useNewUrlParser: true });
    return new Promise((resolve, reject) => {
      client.connect((err, _) => {
        if (err) { reject(err); client.close(); return; }
        const db = client.db('RNdb');
        const collection = db.collection('review');
        collection.find({ itemId: id }).toArray((err, docs) => {
          client.close();
          if (err) { console.log(err); reject(err); } else resolve(docs);
        });
      });
    });
  },
};
