const jwt = require('jsonwebtoken');
const express = require('express');
const crypto = require('crypto');
const mongodb = require('../db/mongodb');

const router = express.Router();

const hashSize = 32,
  hashAlgorithm = 'sha512',
  iterations = 1000,
  jwtSecretKey = '142e6ecf42884f03';
// jelszo: 7f290ed8854edc20507991e2aa81c7d8ab606a48fa6e47fd3a8f0a0c5d001197cebc7dc00c3d776eb01df33172ee066d

function verifyLogin(req, res) {
  const { username, password } = req.body;
  mongodb.getUserByName(username)
    .then((dbResp) => {
      if (typeof (dbResp[0]) === 'undefined') {
        res.status(412).json({ error: 'User does not exist!' });
        return;
      }
      const hashedPassword = dbResp[0].password;
      const requestHash = hashedPassword.substring(0, hashSize * 2);
      const salt = Buffer.from(hashedPassword.substring(hashSize * 2), 'hex');

      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
        if (err) res.status(500).json({ error: `Hashing unsuccessful: ${err.message}` });
        else {
          const actualHash = binaryHash.toString('hex');
          if (requestHash === actualHash) {
            req.session.username = username;
            res.cookie('username', username);
            jwt.sign(username, jwtSecretKey, (errJwt, token) => {
              if (errJwt) res.status(500).json({ error: `Json Web Token error: ${errJwt.message}` });
              res.cookie('jwt', token);
              console.log('Login successful');
              res.json({
                success: 'Login successful',
                token,
                username: dbResp[0].username,
                rentTime: dbResp[0].rentTime,
              });
            });
          } else {
            res.status(412).json({ error: 'The password is incorrect!' });
          }
        }
      });
    })
    .catch(error => res.status(500).json({ error: `Error: ${error.message}` }));
}

router.post('/', (req, res) => {
  const { username, password } = req.body;
  if (username === '' || password === '') {
    res.status(400).json({ error: 'Username or password is empty!' });
    return;
  }
  verifyLogin(req, res);
});

module.exports = router;
