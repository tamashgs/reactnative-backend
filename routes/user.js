const express = require('express');
const mongodb = require('../db/mongodb');

const router = express.Router();

router.get('/:username/history', async (req, res) => {
  const { username } = req.params;
  mongodb.getHistoryByUsername(username)
    .then(dbres => res.json(dbres))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

module.exports = router;
