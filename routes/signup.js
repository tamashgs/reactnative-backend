/* eslint-disable arrow-parens */
/* eslint-disable no-shadow */
const express = require('express');
const crypto = require('crypto');
const mongodb = require('../db/mongodb');

const router = express.Router();

const hashSize = 32,
  saltSize = 16,
  hashAlgorithm = 'sha512',
  iterations = 1000;

function generateHash(password) {
  return new Promise((resolve, reject) => {
    crypto.randomBytes(saltSize, (saltErr, salt) => {
      if (saltErr) reject(new Error(`Salt generation unsuccessful: ${saltErr.message}`));
      crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (cryptErr, hash) => {
        if (cryptErr) reject(new Error(`Hashing unsuccessful: ${cryptErr.message}`));
        const hashWithSalt = Buffer.concat([hash, salt]).toString('hex');
        resolve(hashWithSalt);
      });
    });
  });
}

router.post('/', (req, res) => {
  const {
    username, password, password2, email,
  } = req.body;

  if (username === '' || password === '' || password2 === '' || email === '') {
    res.status(400).json({ error: 'One of the fileds is empty!' });
    return;
  }

  if (password !== password2) {
    res.status(400).json({ error: 'Passwords does not match!' });
    return;
  }

  mongodb.getUserByName(username).then((dbResp) => {
    if (typeof dbResp[0] !== 'undefined') {
      res.status(400).json({ error: 'This username already taken!' });
      return;
    }
    generateHash(password)
      .then(password => {
        const date = (new Date()).getTime();
        mongodb.addUser({
          username, password, email, date, rentTime: null,
        })
          .then(dbResp => {
            res.json({ success: 'Sign up successful', dbResp });
          })
          .catch(addErr => res.status(500).json({ error: `Error in creating user: ${addErr.message}` }));
      })
      .catch(error => res.status(500).json({ error: `Error: ${error.message}` }));
  })
    .catch(error => res.status(500).json({ error: `Error in getting user: ${error.message}` }));
});

module.exports = router;
