const jwt = require('jsonwebtoken');
const express = require('express');
const crypto = require('crypto');
const mongodb = require('../db/mongodb');

const router = express.Router();

const hashSize = 32,
  hashAlgorithm = 'sha512',
  iterations = 1000,
  jwtSecretKey = '142e6ecf42884f03';

const pwd = '7f290ed8854edc20507991e2aa81c7d8ab606a48fa6e47fd3a8f0a0c5d001197cebc7dc00c3d776eb01df33172ee066d';

router.post('/', (req, res) => {
  const { password } = req.body;
  console.log(req.body);
  const hashedPassword = pwd;
  const requestHash = hashedPassword.substring(0, hashSize * 2);
  const salt = Buffer.from(hashedPassword.substring(hashSize * 2), 'hex');

  crypto.pbkdf2(password, salt, iterations, hashSize, hashAlgorithm, (err, binaryHash) => {
    if (err) res.status(500).json({ error: `Hashing unsuccessful: ${err.message}` });
    else {
      const actualHash = binaryHash.toString('hex');
      if (requestHash === actualHash) {
        jwt.sign('admin', jwtSecretKey, (errJwt, token) => {
          if (errJwt) res.status(500).json({ error: `Json Web Token error: ${errJwt.message}` });
          res.cookie('jwt', token);
          res.json({
            token,
          });
        });
      } else {
        res.status(412).json({ error: 'The password is incorrect!' });
      }
    }
  });
});

module.exports = router;
