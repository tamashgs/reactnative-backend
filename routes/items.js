const express = require('express');
const mongodb = require('../db/mongodb');

const router = express.Router();

router.get('/', (req, res) => {
  mongodb.getAllLocations()
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  mongodb.getItem(id)
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.post('/create', (req, res) => {
  const item = {
    name: req.body.name,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    available: req.body.available,
    code: req.body.code,
    price: req.body.price,
    type: req.body.type,
  };
  mongodb.addItem(item)
    .then(dbResp => res.json({ id: dbResp }))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.post('/update:id', (req, res) => {
  const { id } = req.params;
  mongodb.updateItem(
    id,
    req.body.name === '' ? null : req.body.name,
    req.body.latitude === '' ? null : req.body.latitude,
    req.body.longitude === '' ? null : req.body.longitude,
    req.body.available === '' ? null : req.body.available,
    req.body.code === '' ? null : req.body.code,
    req.body.price === '' ? null : req.body.price,
    req.body.type === '' ? null : req.body.type,
  ).then(dbResp => res.json({ id: dbResp }))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.post('/delete:id', (req, res) => {
  const { id } = req.params;
  mongodb.deleteItem(id)
    .then(dbResp => res.json({ id: dbResp }))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

module.exports = router;
