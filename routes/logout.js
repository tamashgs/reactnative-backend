const express = require('express');

const router = express.Router();

router.get('/', (req, res) => {
  res.clearCookie('jwt-token');
  req.session.destroy();
  res.send({ message: 'Logout succes!' });
});

module.exports = router;
