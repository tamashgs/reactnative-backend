const express = require('express');
const mongodb = require('../db/mongodb');

const router = express.Router();

router.get('/', (req, res) => {
  mongodb.getAll('RNdb', 'review')
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.get('/item:id', (req, res) => {
  const { id } = req.params;
  mongodb.getReviewsByItemId(id)
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.post('/create', (req, res) => {
    console.log(req.body);
    const { username, itemId, rating, msg } = req.body
    const review = {
      username,
      itemId,
      date: (new Date()).getTime(),
      rating: parseInt(rating),
      content: msg,
    }
    console.log(review);
  mongodb.addReview(review)
    .then(dbResp => res.json({ id: dbResp }))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

module.exports = router;
