const express = require('express');
const mongodb = require('../db/mongodb');

const router = express.Router();

router.patch('/locations/:id/users/:username/start', async (req, res) => {
  const { username } = req.params;
  const { location } = req.body;
  const itemID = req.params.id;
  const time = (new Date()).getTime();

  // add a history document
  const historyID = await mongodb.addToHistory(username, itemID, time, location);
  // set users active rental
  const promiseUsr = mongodb.updateUserRental(username, time, itemID, historyID);
  // set the selected item unavailable
  const promiseLoc = mongodb.updateLocationAvailability(itemID, false);

  Promise.all([promiseLoc, promiseUsr])
    .then((resp) => {
      const respBody = {
        rentTime: time,
        rentID: resp[2],
      };
      res.json(respBody);
    })
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.patch('/:username/stop', async (req, res) => {
  const { username } = req.params;
  const { location } = req.body;
  const time = (new Date()).getTime();

  const price = 1;
  // get user data for start time
  const userInfo = await mongodb.getUserByName(username);
  // set the selected item available
  const promiseLoc = mongodb.updateLocationAvailability(userInfo[0].rentItemId, true);
  // active rental set null
  const promiseUsrUpt = mongodb.updateUserRental(username, null, null);
  // update the rental history - finish time and location
  const amount = (time - userInfo[0].rentTime) / 60000 * price;
  const promiseHist = mongodb.updateHistory(
    username,
    userInfo[0].rentId,
    userInfo[0].rentItemId,
    time,
    location,
    amount,
  );

  Promise.all([promiseLoc, promiseUsrUpt, promiseHist])
    .then(() => res.json(amount))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

module.exports = router;
