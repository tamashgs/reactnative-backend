const express = require('express');
const mongodb = require('../db/mongodb');

const router = express.Router();

router.get('/', (req, res) => {
  mongodb.getAllItemsWithRate()
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.get('/:id', (req, res) => {
  const { id } = req.params;
  mongodb.getItem(id)
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

router.get('/simple', (req, res) => {
  mongodb.getAll('RNdb','locations')
    .then(resp => res.json(resp))
    .catch(error => res.status(500).json({ error: `Error in database: ${error}` }));
});

module.exports = router;
